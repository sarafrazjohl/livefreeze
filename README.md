# README #

Micro-framework for rapid delivery & caching of web resources.

### What does deliveree do? ###

* Minification
* Hot-link protection
* On-demand merge
* Caching and Automatic cache-breaking

### How do I get set up? ###

* Move 'delivee' directory to your webroot (or webpath)
* Configure options in delivee.conf.php

### How do I use it? ###

* Follow steps in above section to set it up
* Start linking resources as shown below
	* <link rel="stylesheet" type="text/css" href="/deliveree/?css=*common+alternate*"/>
	* <script type="text/javascript" src="/deliveree/?js=*jquery+bootstrap*"></script>

### Who do I talk to? ###

* [Sarafraz Johl](http://www.sarafraz.com/)